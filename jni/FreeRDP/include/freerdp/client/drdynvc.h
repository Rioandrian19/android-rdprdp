/**
 * FreeRDP: A Remote Desktop Protocol Client
 * Input Virtual Channel Extension Types
 *
 * Copyright 2011 Vic Lee
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __DRDYNVC_PLUGIN
#define __DRDYNVC_PLUGIN

/**
 * Event Types
 */
enum RDP_EVENT_TYPE_DRDYNVC
{
	RDP_EVENT_TYPE_DRDYNVC_INPUT = 1,
};

struct _RDP_DRDYNVC_EVENT
{
	RDP_EVENT event;
};

typedef struct _RDP_DRDYNVC_EVENT RDP_DRDYNVC_EVENT;

#define CONTACT_DATA_CONTACTRECT_PRESENT    0x0001
#define CONTACT_DATA_ORIENTATION_PRESENT    0x0002
#define CONTACT_DATA_PRESSURE_PRESENT       0x0004

#define CONTACT_FLAG_DOWN       0x0001
#define CONTACT_FLAG_UPDATE     0x0002
#define CONTACT_FLAG_UP         0x0004
#define CONTACT_FLAG_INRANGE    0x0008
#define CONTACT_FLAG_INCONTACT  0x0010
#define CONTACT_FLAG_CANCELED   0x0020

#define MAX_TOUCH_CONTACTS      5
#define MAX_TOUCH_FRAMES        64

struct _RDP_DRDYNVC_CONTACT_DATA
{
    UINT8 contactId;
    UINT32 fieldsPresent;
    INT32   x;
    INT32   y;
    UINT32 contactFlags;
    INT16 contactRectLeft;
    INT16 contactRectTop;
    INT16 contactRectRight;
    INT16 contactRectBottom;
    UINT32 orientation;
    UINT32 pressure;
};
typedef struct _RDP_DRDYNVC_CONTACT_DATA RDP_DRDYNVC_CONTACT_DATA;

struct _RDP_DRDYNVC_INPUT_TOUCH_FRAME
{
    UINT16 contactCount;
    UINT64 frameOffset;
    RDP_DRDYNVC_CONTACT_DATA contacts[MAX_TOUCH_CONTACTS];
};

typedef struct _RDP_DRDYNVC_INPUT_TOUCH_FRAME RDP_DRDYNVC_INPUT_TOUCH_FRAME;

struct _RDP_DRDYNVC_INPUT_EVENT
{
    RDP_DRDYNVC_EVENT event;
    UINT32 encodeTime;
    UINT16 frameCount;
    RDP_DRDYNVC_INPUT_TOUCH_FRAME frames[MAX_TOUCH_FRAMES];
};

typedef struct _RDP_DRDYNVC_INPUT_EVENT RDP_DRDYNVC_INPUT_EVENT;

#endif /* __DRDYNVC_PLUGIN */

